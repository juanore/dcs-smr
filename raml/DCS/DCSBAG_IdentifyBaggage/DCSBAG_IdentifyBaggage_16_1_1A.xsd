<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://xml.amadeus.com/BIDBRQ_16_1_1A" xmlns="http://xml.amadeus.com/BIDBRQ_16_1_1A" elementFormDefault="qualified">
  <xs:element name="DCSBAG_IdentifyBaggage">
    <xs:annotation>
      <xs:documentation xml:lang="en">Queries DCS Server to retrieve bags depending on the Criteria contained in the message.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="identifyBaggageIndicators" minOccurs="0" maxOccurs="2" type="StatusTypeI">
          <xs:annotation>
            <xs:documentation xml:lang="en">Identify baggage indicators: EMA: Exact match indicator RCD: Responsible customer details</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="responsibleCustProduct" minOccurs="0" maxOccurs="99" type="ItemReferencesAndVersionsType">
          <xs:annotation>
            <xs:documentation xml:lang="en">This is used to specify the unique product identifiers of the responsible customers.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="responsibleCustomerDetails" minOccurs="0" maxOccurs="99">
          <xs:annotation>
            <xs:documentation xml:lang="en">Used to convey information about a responsible customer.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="customerDetails" type="TravellerInformationType">
                <xs:annotation>
                  <xs:documentation xml:lang="en">To specify: - Customer name  - Customer surname</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="enhancedCustomerData" minOccurs="0" type="EnhancedTravellerInformationType">
                <xs:annotation>
                  <xs:documentation xml:lang="en">Univeral and Romanised name</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="responsibleCustomerUCI" minOccurs="0" type="ItemReferencesAndVersionsType">
                <xs:annotation>
                  <xs:documentation xml:lang="en">Used to convey the UCI of the responsible customer.</xs:documentation>
                </xs:annotation>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="uniqueBagIdentifier" minOccurs="0" maxOccurs="200" type="ReferenceInformationTypeI">
          <xs:annotation>
            <xs:documentation xml:lang="en">To convey the Unique Bag Identifier of the Bags to identify. (1153 contain UBI)</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="bagTagDetails" minOccurs="0" type="BaggageInformationTypeI">
          <xs:annotation>
            <xs:documentation xml:lang="en">Used to convey bag tag details.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="permanentBagTag" minOccurs="0">
          <xs:annotation>
            <xs:documentation xml:lang="en">to convey the permanent bag tag details:  Issuing carrier code Permanent bag tag number</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="pbtIssuingCarrierCode" type="TransportIdentifierType">
                <xs:annotation>
                  <xs:documentation xml:lang="en">to hold the issuing carrier code of the permanent bag tag</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="permanentBagTag" type="ReferenceInformationTypeI_81782S">
                <xs:annotation>
                  <xs:documentation xml:lang="en">to hold the permanent bag tag number</xs:documentation>
                </xs:annotation>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="baggageGroupInfo" minOccurs="0">
          <xs:annotation>
            <xs:documentation xml:lang="en">Used to convey the baggage reference for the bagsgroup that we want to identify and / or the baggage type.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="dum" type="DummySegmentTypeI">
                <xs:annotation>
                  <xs:documentation xml:lang="en">Dummy segment because no other segment is Mandatory.</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="baggageReference" minOccurs="0" maxOccurs="200" type="ItemReferencesAndVersionsType">
                <xs:annotation>
                  <xs:documentation xml:lang="en">Used to convey the baggage reference of the bagsgroup.</xs:documentation>
                </xs:annotation>
              </xs:element>
              <xs:element name="baggageType" minOccurs="0" type="StatusTypeI">
                <xs:annotation>
                  <xs:documentation xml:lang="en">Used to specify the baggage type.</xs:documentation>
                </xs:annotation>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="routingDetails" minOccurs="0" type="FlightDetailsResponseType">
          <xs:annotation>
            <xs:documentation xml:lang="en">This is to specify the routing of the baggage</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="historicalDays" minOccurs="0" type="NumberOfUnitsType">
          <xs:annotation>
            <xs:documentation xml:lang="en">This is to indicate the number of days to search back in time.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="rawData" minOccurs="0" type="BinaryDataType">
          <xs:annotation>
            <xs:documentation xml:lang="en">Binary raw data form the swiped card Card that can be swiped Credit card FQTV Card Ticket Boarding Pass</xs:documentation>
          </xs:annotation>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="BaggageInformationTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To provide information concerning bagtag printers and bagtag details.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="baggageDetails" minOccurs="0" type="BaggageStatusDetailsTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">Contains the details of an individual bag.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="bagTagDetails" minOccurs="0" type="BagtagDetailsTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">Contains the bag tag details</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="individualWeight" minOccurs="0" type="CheckedBaggageDetailsType">
        <xs:annotation>
          <xs:documentation xml:lang="en">Individual bag weight information</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="BaggageStatusDetailsTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To identify a printer I.D. and a cabin class.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="cabinClass" minOccurs="0" type="AlphaString_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">Cabin of the bag</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="status" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">This is used to indicate  - the bag source type, 'statusIndicator' containing: "BSA" for automatic "BSB" for BTM "BSI" for IATCI</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="BagtagDetailsTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To identify baggage by company identification, serial numbers, and destination.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="company" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">Airline code for the company issuing the tag.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="identifier" minOccurs="0" type="NumericInteger_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">Standard IATA 1 digit code used to refer to the type of a bag tag.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="number" minOccurs="0" type="NumericInteger_Length6To6">
        <xs:annotation>
          <xs:documentation xml:lang="en">Bag Tag number (6 generated digits).</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="location" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">Bag tag final destination.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="companyNumber" minOccurs="0" type="NumericInteger_Length3To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">Airline number for the company issuing the tag.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="indicator" minOccurs="0" type="NumericInteger_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">Transfer tag indicator</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="characteristic" minOccurs="0" type="AlphaNumericString_Length3To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">This is used to indicate the bag tag source type, 'statusIndicator' containing: "TSA" for automatic "TSB" for BTM "TSI" for IATCI "TSM" for Manual</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="specialRequirement" minOccurs="0" type="AlphaNumericString_Length1To4">
        <xs:annotation>
          <xs:documentation xml:lang="en">Bag exception data.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="measurement" minOccurs="0" type="NumericInteger_Length1To4">
        <xs:annotation>
          <xs:documentation xml:lang="en">Contains the individual weight of a bag.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="unitQualifier" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">Contains the weight unit</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="description" minOccurs="0" type="AlphaNumericString_Length1To70">
        <xs:annotation>
          <xs:documentation xml:lang="en">Contains the bag comment.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="julianDate" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">AirFrance: Julian date in the current year to print on the bagtag (0-366)</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="BinaryDataType">
    <xs:annotation>
      <xs:documentation xml:lang="en">to carry binary data within an Edifact segment</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="dataLength" type="NumericInteger_Length1To15">
        <xs:annotation>
          <xs:documentation xml:lang="en">Length of 203K</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="dataType" minOccurs="0" type="AlphaNumericString_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">type of the data</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="binaryData" type="AlphaNumericString_Length1To9999">
        <xs:annotation>
          <xs:documentation xml:lang="en">used to store binary data</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CheckedBaggageDetailsType">
    <xs:annotation>
      <xs:documentation xml:lang="en">CHECKED BAGGAGE DETAILS</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="weight" minOccurs="0" type="NumericInteger_Length1To4">
        <xs:annotation>
          <xs:documentation xml:lang="en">To specify the total Weight of Checked Baggage.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="unitQualifier" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">To convey the unit in which the weight is given.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="weightUnknown" minOccurs="0" type="AlphaNumericString_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">Y: individual weight unknown N: individual weight known</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="CompanyIdentificationTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">Code or name to identify a company and any associated companies.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="marketingCompany" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">This is used to convey a marketing carrier code.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="DummySegmentTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To serve the purpose of a mandatory segment at the beginning of a group and to avoid segment collision.</xs:documentation>
    </xs:annotation>
    <xs:sequence />
  </xs:complexType>
  <xs:complexType name="EnhancedTravellerInformationType">
    <xs:annotation>
      <xs:documentation xml:lang="en">To specify traveler and personal details relating to one traveler having rich name and/or multiple names</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="travellerNameInfo" minOccurs="0" type="TravellerNameInfoType">
        <xs:annotation>
          <xs:documentation xml:lang="en">Name attributes unique for one passenger.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="otherPaxNamesDetails" minOccurs="0" maxOccurs="5" type="TravellerNameDetailsType">
        <xs:annotation>
          <xs:documentation xml:lang="en">5 possible types of names, for 1 passenger.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="FlightDetailsResponseType">
    <xs:annotation>
      <xs:documentation xml:lang="en">To specify outbound flight details.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="carrierDetails" minOccurs="0" type="OutboundCarrierDetailsTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">Carrier details</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="flightDetails" minOccurs="0" type="OutboundFlightNumberDetailstypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">Toc specify the flight number details</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="departureDate" minOccurs="0" type="Date_YYYYMMDD">
        <xs:annotation>
          <xs:documentation xml:lang="en">Departure date.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="boardPoint" minOccurs="0" type="AlphaString_Length1To5">
        <xs:annotation>
          <xs:documentation xml:lang="en">Departure location</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="offPoint" minOccurs="0" type="AlphaString_Length1To5">
        <xs:annotation>
          <xs:documentation xml:lang="en">Arrival location</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="arrivalDate" minOccurs="0" type="Date_YYYYMMDD">
        <xs:annotation>
          <xs:documentation xml:lang="en">Arrival Date</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ItemReferencesAndVersionsType">
    <xs:annotation>
      <xs:documentation xml:lang="en">Exchange and link unique identifiers</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="idSection" type="UniqueIdDescriptionType">
        <xs:annotation>
          <xs:documentation xml:lang="en">ID details</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="NumberOfUnitDetailsTypeI">
    <xs:sequence>
      <xs:element name="numberOfUnit" type="NumericInteger_Length1To4">
        <xs:annotation>
          <xs:documentation xml:lang="en">Number of Units.</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="unitQualifier" type="AlphaNumericString_Length3To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">Qualifier of the type of Units</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="NumberOfUnitsType">
    <xs:annotation>
      <xs:documentation xml:lang="en">To specify the number of units required</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="quantityDetails" type="NumberOfUnitDetailsTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">Number of Unit Details</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OutboundCarrierDetailsTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To identify the airline company and an associated airline company.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="marketingCarrier" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">The carrier code</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="otherCarrier" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">This data element is used to send the second carrier is case of OPEN segment.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="OutboundFlightNumberDetailstypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To specify the flight number and operational suffix of the outbound flight.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="flightNumber" type="AlphaNumericString_Length1To4">
        <xs:annotation>
          <xs:documentation xml:lang="en">The flight number</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="operationalSuffix" minOccurs="0" type="AlphaString_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">The operational suffix</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ReferenceInformationTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To provide specific reference identification for a traveller.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="referenceDetails" type="ReferencingDetailsTypeI_183272C">
        <xs:annotation>
          <xs:documentation xml:lang="en">Reference details</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ReferenceInformationTypeI_81782S">
    <xs:annotation>
      <xs:documentation xml:lang="en">To provide specific reference identification for a traveller.</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="referenceDetails" type="ReferencingDetailsTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">Reference details</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ReferencingDetailsTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">Reference details</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="type" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">To specify the type of Identifier conveyed in the 1154</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="value" type="AlphaNumericString_Length1To35">
        <xs:annotation>
          <xs:documentation xml:lang="en">To specify a DCS Internal Unique Identifier</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="ReferencingDetailsTypeI_183272C">
    <xs:annotation>
      <xs:documentation xml:lang="en">Reference details</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="type" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">To specify the type of Identifier conveyed in the 1154</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="value" type="AlphaNumericString_Length1To16">
        <xs:annotation>
          <xs:documentation xml:lang="en">To specify a DCS Internal Unique Identifier</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="StatusDetailsTypeI">
    <xs:sequence>
      <xs:element name="indicator" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">StatusIndicator</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="action" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">To Specify Yes or No.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="StatusTypeI">
    <xs:annotation>
      <xs:documentation xml:lang="en">To advise the requester system the status of the reply</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="statusDetails" type="StatusDetailsTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">Status Information, Indicators.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TransportIdentifierType">
    <xs:annotation>
      <xs:documentation xml:lang="en">To specify the transport service(s) which is /are to be updated or cancelled</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="companyIdentification" type="CompanyIdentificationTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">companyIdentification</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TravellerDetailsTypeI">
    <xs:sequence>
      <xs:element name="givenName" minOccurs="0" type="AlphaNumericString_Length1To70">
        <xs:annotation>
          <xs:documentation xml:lang="en">Passenger  given name and title</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="type" minOccurs="0" type="AlphaNumericString_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">Gender of the Customer</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="title" minOccurs="0" type="AlphaNumericString_Length1To20">
        <xs:annotation>
          <xs:documentation xml:lang="en">Title of the Customer.</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TravellerInformationType">
    <xs:annotation>
      <xs:documentation xml:lang="en">To specify a traveler and personal details relating to the traveler</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="paxDetails" type="TravellerSurnameInformationType">
        <xs:annotation>
          <xs:documentation xml:lang="en">Traveller Surname Information</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="otherPaxDetails" minOccurs="0" type="TravellerDetailsTypeI">
        <xs:annotation>
          <xs:documentation xml:lang="en">First Name</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TravellerNameDetailsType">
    <xs:annotation>
      <xs:documentation xml:lang="en">To identify all the names in different alphabets associated to a same traveller</xs:documentation>
    </xs:annotation>
    <xs:sequence>
      <xs:element name="nameType" type="AlphaNumericString_Length1To5" />
      <xs:element name="referenceName" minOccurs="0" type="AlphaNumericString_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">Y = Indicates that the name is the original name (not the DCS name)</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="displayedName" minOccurs="0" type="AlphaNumericString_Length1To1">
        <xs:annotation>
          <xs:documentation xml:lang="en">Y = indicates this is the DCS Name</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="romanizationMethod" minOccurs="0" type="AlphaNumericString_Length1To4" />
      <xs:element name="surname" minOccurs="0" type="AlphaNumericString_Length1To70">
        <xs:annotation>
          <xs:documentation xml:lang="en">Passenger surname</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="givenName" minOccurs="0" type="AlphaNumericString_Length1To70">
        <xs:annotation>
          <xs:documentation xml:lang="en">Passenger firstname</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="title" minOccurs="0" maxOccurs="2" type="AlphaNumericString_Length1To70" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TravellerNameInfoType">
    <xs:sequence>
      <xs:element name="quantity" type="NumericInteger_Length1To15">
        <xs:annotation>
          <xs:documentation xml:lang="en">1 :one traveler with exceptions below. 2 :traveler accompanied by an infant. n : total number of passengers of the group (assigned + unassigned).</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="type" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">Passenger type (PTC).</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="infantIndicator" minOccurs="0" type="AlphaNumericString_Length1To1" />
      <xs:element name="travellerIdentificationCode" minOccurs="0" type="AlphaNumericString_Length1To70" />
      <xs:element name="gender" minOccurs="0" type="AlphaNumericString_Length1To3" />
      <xs:element name="age" minOccurs="0" type="NumericInteger_Length1To3" />
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="TravellerSurnameInformationType">
    <xs:sequence>
      <xs:element name="surname" type="AlphaNumericString_Length1To70">
        <xs:annotation>
          <xs:documentation xml:lang="en">Passenger name</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="type" minOccurs="0" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">Customer type: A=adult C=child IN = infant</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="quantity" minOccurs="0" type="NumericInteger_Length1To2">
        <xs:annotation>
          <xs:documentation xml:lang="en">Age of the unaccompanied minor</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:complexType name="UniqueIdDescriptionType">
    <xs:sequence>
      <xs:element name="referenceQualifier" type="AlphaNumericString_Length1To3">
        <xs:annotation>
          <xs:documentation xml:lang="en">ID qualifier</xs:documentation>
        </xs:annotation>
      </xs:element>
      <xs:element name="primeId" type="AlphaNumericString_Length16To16">
        <xs:annotation>
          <xs:documentation xml:lang="en">ID (DID, IID, SID number or any uniquer identifier)</xs:documentation>
        </xs:annotation>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
  <xs:simpleType name="AlphaString_Length1To5">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: a..5</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="5" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericInteger_Length1To15">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: n..15</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:pattern value="-?[0-9]{1,15}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To1">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an1</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="1" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To9999">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..9999</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="9999" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..3</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="3" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length16To16">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an16</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="16" />
      <xs:maxLength value="16" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To70">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..70</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="70" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericInteger_Length1To2">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: n..2</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:pattern value="-?[0-9]{1,2}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To20">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..20</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="20" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericInteger_Length1To3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: n..3</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:pattern value="-?[0-9]{1,3}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To5">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..5</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="5" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To4">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..4</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="4" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To16">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..16</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="16" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaString_Length1To1">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: a1</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="1" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericInteger_Length1To1">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: n1</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:pattern value="-?[0-9]{1,1}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericInteger_Length6To6">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: n6</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:pattern value="-?[0-9]{6,6}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericInteger_Length3To3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: n3</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:pattern value="-?[0-9]{3,3}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length3To3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an3</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="3" />
      <xs:maxLength value="3" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericInteger_Length1To4">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: n..4</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:pattern value="-?[0-9]{1,4}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="AlphaNumericString_Length1To35">
    <xs:annotation>
      <xs:documentation xml:lang="en">Format limitations: an..35</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="35" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Date_YYYYMMDD">
    <xs:annotation>
      <xs:documentation xml:lang="en">Date format: YYYYMMDD</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])" />
    </xs:restriction>
  </xs:simpleType>
</xs:schema>
<!-- SchemaRules="2010/01"-->
<!--Created with SchemaBuilder 5.1.0 by Amadeus S.A.S.-->
<!--"Amadeus Proprietary and Confidential Information: unauthorized use and disclosure strictly forbidden. 2010 - Amadeus s.a.s - All Rights Reserved."-->
<!--Build Key:
1F8B08000000000000009553CB6EDB3010BCF32B885E7A282CCB4190B406789023C7085027A9EDDC0C18B4B8968950A4CAA582A841FEBD4B496ED243D1F622EC83DAC7CCEC627E3B5F659B79BEBBBB1567E9E472949E8F26295F3AAB64CBD38BE9D9E7E9F917BE586E3EA5E9344D59B6CC425B035E6B0356562028B09B49D4C526869367546C2F116E2987B52C401C43A8A7E3F17365125949050D2685ABC66CDF586520D75E8C1142D0B6C4B1D205B2E208C5E39ABE9514C137C00AA7809EA0B0CE46AFAAC09237E4AA5ADA36B613B631860275EB75790CE243D6B7E3F7DED55E4390BEE5D22A7EE5EC412BAAA1A5E137F6E07C25837676CA1B2B9B70745EFF00C51B84EEB9D25818878D078EC1EB229896D33F7BADA846C209B6948FF8A9192632C1E81BC357710EE42B9ADE3F814A3E300507D998B0F075844B898334084CB9666F600DDF1BB005DCCB501C870CD8D93F8109F6EDCD4B24E875F7F2041E692BB23C18A03264C9923AB4AFEC40F4AD1A4398B9C864E77774763F47EB2F058C2BA4192037AE5444633EDD1E41AA2D287D904508CE19DCCE37BB5C6390365C1153C41F31B75D4AFF0891F14DF7063BAE71FBD59554CB96DD205ADD59D3B20A7C0903189D7DADC1285C3B1F7EC1478B23CD9561C4F4F7584EAC6B3328C7FE1744B5D336DC42E8E9E8C4E69D0BEF10EAFC28E084F52B444431DE513A4E27430CAF9DCF8CA6DA8362A3167401989392A83789B247112BE943ACFB6075A1433B2C12BCB4683A7D123E82B0C6F15079932DC72F0A8CA6352409E835A12C8BE3A9F7E74362B93ADD4C5F93221D54DD256B5BB06734E1AE097513AE9D51E0450C30361A2D7B183F22D76F77321AB11E2431C9D8BBFE22BBCF76F9D57A962D76938BF33DE9BA92DA0A0A3178AE89B29DC45D3E5F88CDEA617EA2884B0F525C9E9D7C31BBC967AB6FACA69BD07824967BEC7B82C484AC271D5923B3034776388AA1F14D77D8877626CB92AAB1135B7FCA0F1210930BF613857E1CA20E050000
-->

